<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 13:51
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Highloadblock as HL;

class Materials extends CBitrixComponent
{
    /**
     * @var array $defaultUrlTemplates шаблон адреса
     */
    public $defaultUrlTemplates = [
        'index' => 'index.php',
        'element' => '#ID#',
        'comments' => 'comments/#ID#'
    ];

    /**
     * @var array $variables массив параметр => значение из свойства $defaultUrlTemplates404
     */
    public $variables = [];

    /**
     * @var array $componentVariables массив параметр => значение для компонента
     */
    public $componentVariables = [];

    /**
     * @var array $variableAliases алиасы значений
     */
    public $variableAliases = [];

    /**
     * @var array $templatesUrls сформированный массив шаблон адреса
     */
    public $templatesUrls = [];

    /**
     * @var string $componentPage шаблон отрисовки
     */
    public $componentPage = '';

    /**
     * Иницилизация объектов необходимые для работы
     * @return bool
     */
    protected function InitializeComponent()
    {
        $this->setFrameMode(true);
        if (!CModule::IncludeModule("iblock")) {
            return false;
        }

        if (!CModule::IncludeModule('highloadblock')) {
            return false;
        }

        $this->templatesUrls = CComponentEngine::makeComponentUrlTemplates($this->defaultUrlTemplates, $this->arParams["SEF_FOLDER"]);

        $this->componentPage = CComponentEngine::parseComponentPath(
            $this->arParams["SEF_FOLDER"],
            $this->templatesUrls,
            $this->variables
        );
        CComponentEngine::initComponentVariables($this->componentPage, $this->componentVariables, $this->variableAliases, $this->variables);

        return true;
    }

    /**
     * Выполнение компонента, главный метод
     * @return mixed|void
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        $this->InitializeComponent();

        if ($this->componentPage != 'comments') {
            $result = $this->getData();
        } else {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                //add comment
                $this->addComment($_REQUEST['id'], $_REQUEST['comment']);
            } else {
                $result = $this->getComments();
            }
        }

        $this->arResult = array_merge(
            [
                'VARIABLES' => $this->variables,
                'DATA' => $result,
            ],
            $this->arResult
        );

        $this->includeComponentTemplate($this->componentPage);
    }

    /**
     * Обработчиик получения данных
     * @return array|mixed|null
     */
    public function getData()
    {
        $data = null;

        $data = $this->getElementOrElements($this->componentPage == 'index' ? null : $this->variables['ID']);

        return $data;
    }

    /**
     * Получение данных элемента или элементов
     * @param null $id
     * @return array|mixed
     */
    public function getElementOrElements($id = null)
    {
        $data = [];
        $arFilter = [
            'IBLOCK_CODE' => 'materials'
        ];
        $arSelect = [
            'NAME',
            'ID',
            'PREVIEW_TEXT',
            'PREVIEW_PICTURE',
            'DETAIL_PICTURE',
            'DETAIL_TEXT',
        ];

        if ($id) {
            $arFilter['ID'] = $id;
        }

        $elements = CIBlockElement::GetList(["SORT"=>"ASC"], $arFilter, false, false, $arSelect);

        while($element = $elements->GetNext()) {
            $resizePreviewPictures = CFile::ResizeImageGet($element['PREVIEW_PICTURE'],
                [
                    'width'=>isset($this->arParams["PREVIEW"]['WIDTH']) ? $this->arParams["PREVIEW"]['WIDTH'] : 128,
                    'height'=>isset($this->arParams["PREVIEW"]['HEIGHT']) ? $this->arParams["PREVIEW"]['HEIGHT'] : 128
                ], BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $resizeDetailPictures = CFile::ResizeImageGet($element['DETAIL_PICTURE'],
                [
                    'width'=>isset($this->arParams["DETAIL"]['WIDTH']) ? $this->arParams["DETAIL"]['WIDTH'] : 512,
                    'height'=>isset($this->arParams["DETAIL"]['WIDTH']) ? $this->arParams["DETAIL"]['WIDTH'] : 512
                ]
                , BX_RESIZE_IMAGE_PROPORTIONAL, true);

            $data[] = [
                'title' => $element['NAME'],
                'preview_pictures' => $resizePreviewPictures ? $resizePreviewPictures['src'] : null,
                'preview_text' => $element['PREVIEW_TEXT'],
                'detail_pictures' => $resizeDetailPictures ? $resizeDetailPictures['src'] : null,
                'detail_text' => $element['DETAIL_TEXT'],
                'url' => $this->arParams["SEF_FOLDER"] . $element['ID'],
                'comment_url' => $this->arParams["SEF_FOLDER"] . 'comments/' .  $element['ID'],
            ];
        }

        return $id ? array_shift($data) : $data;
    }

    /**
     * Получение комментариев
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getComments()
    {
        $id = $this->variables['ID'];

        $entityClass = $this->initHL();

        $res = $entityClass::getList([
            'select' => ['*'],
            'filter' => ['UF_ID' => $id],
        ]);

        $rows = $res->fetchAll();

        $comments = [];

        foreach ($rows as $row) {
            $comments[] = $row['UF_COMMENT'];
        }

        return $comments;
    }

    public function addComment($elementId, $comment)
    {
        $entityClass = $this->initHL();

        $resultAdd = $entityClass::add([
            'UF_COMMENT' => $comment,
            'UF_ID' => $elementId,
        ]);

        md5('wfwef');
    }

    /**
     * Иницилизация HL
     * @return \Bitrix\Main\ORM\Data\DataManager
     * @throws \Bitrix\Main\SystemException
     */
    public function initHL()
    {
        $block = HL\HighloadBlockTable::getList(['filter' => ['TABLE_NAME' => 'materialscomments']])->fetch();

        $hlblock = HL\HighloadBlockTable::getById($block['ID'])->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

        return $entityClass;
    }
}