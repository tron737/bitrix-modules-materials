<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 14:09
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


foreach ($arResult['DATA'] as $element) {
    ?>

    <div class="clearfix">
        <div class="left">
            <? if ($element['preview_pictures']) { ?>
                <img src="<?= $element['preview_pictures'] ?>">
            <? } ?>
        </div>
        <div class="left">
            <div><a href="<?= $element['url'] ?>"><?= $element['title'] ?></a></div>
            <div><?= $element['preview_text'] ?></div>
        </div>
    </div>

    <?
}

