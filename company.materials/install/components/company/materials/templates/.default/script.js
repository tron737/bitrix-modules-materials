if (window.jQuery) {
    $(document).ready(() => {
        $('[js-btn-get-comments]').on('click', reloadComments);

        $('#comments').on('submit', '[js-form-add-comment]', (e) => {
            BX.ajax({
                url: comment_url,
                method: 'POST',
                data: $(e.target).closest('form').serialize(),
                onsuccess: () => {
                    reloadComments();
                },
            });

            return false;
        });
    });
}

function reloadComments()
{
    BX.ajax({
        url: comment_url,
        method: 'GET',
        onsuccess: (data) => {
            $('#comments').empty().html(data);
        }
    });
}