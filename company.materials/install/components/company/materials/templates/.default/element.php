<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 14:52
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array("jquery"));

?>
<script>
    var comment_url = '<?= $arResult['DATA']['comment_url']?>';
</script>
<div>
    <h1><?= $arResult['DATA']['title'] ?></h1>
    <div><img src="<?= $arResult['DATA']['detail_pictures'] ?>"></div>
    <div><?= $arResult['DATA']['detail_text'] ?></div>
    <div><a href="javascript:;" js-btn-get-comments data-id="<?= $arResult['VARIABLES']['ID'] ?>">Показать комментарии</a></div>
    <div id="comments"></div>
</div>
