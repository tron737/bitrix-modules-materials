<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 18:40
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;

class company_materials extends CModule
{
    public $MODULE_ID = 'company.materials';
    public $MODULE_NAME = 'company.materials';
    public $pathResources = 'local';
    public $componentPath = '/components/company/materials';
    public $apiPath = '/materials';
    public $hlTableName = 'materialscomments';
    public $iblockCode = 'materials';

    public $urlRules = [
        "CONDITION" => '#^/materials/#',
        "ID" => 'company:materials',
        "PATH" => '/materials/index.php',
        "RULE" => '',
    ];

    public function __construct()
    {
        if(file_exists(__DIR__."/version.php")) {
            $arModuleVersion = array();

            include_once(__DIR__."/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("MATERIALS_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("MATERIALS_DESCRIPTION");
            $this->PARTNER_NAME = Loc::getMessage("MATERIALS_PARTNER_NAME");
            $this->PARTNER_URI = Loc::getMessage("MATERIALS_PARTNER_URI");
        }

        return false;
    }

    public function DoInstall()
    {
        global $APPLICATION;

        if(CheckVersion(ModuleManager::getVersion("main"), "14.00.00")){

            $this->InstallFiles();
            $this->InstallRouter();
            $this->InstallDB();

            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallEvents();
        }else{

            $APPLICATION->ThrowException(
                Loc::getMessage("MATERIALS_INSTALL_ERROR_VERSION")
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("MATERIALS_INSTALL_TITLE")." \"".Loc::getMessage("MATERIALS_NAME")."\"", __DIR__."/step.php"
        );

        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallRouter();
        $this->UnInstallDB();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("MATERIALS_UNINSTALL_TITLE") . " \"" . Loc::getMessage("MATERIALS_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );

        return false;
    }
    
    public function UnInstallFiles()
    {
        \Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"] . $this->apiPath);
        \Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"] . "/" . $this->pathResources . $this->componentPath);
    }

    public function UnInstallDB()
    {
        if (CModule::IncludeModule('highloadblock')) {
            $hlBlock = Bitrix\Highloadblock\HighloadBlockTable::getList(['filter' => ['TABLE_NAME' => $this->hlTableName]])->fetch();
            if ($hlBlock) {
                Bitrix\Highloadblock\HighloadBlockTable::delete($hlBlock['ID']);
            }
        }
        if (CModule::IncludeModule("iblock")) {
            $iblock = CIBlock::GetList([], ['CODE' => $this->iblockCode], true);
            $iblocData = $iblock->Fetch();
            CIBlock::Delete($iblocData['ID']);
        }
        Option::delete($this->MODULE_ID);
        return false;
    }

    public function InstallFiles()
    {
        if(\Bitrix\Main\IO\Directory::isDirectoryExists(__DIR__ . $this->componentPath)) {
            CopyDirFiles(__DIR__ . $this->componentPath, $_SERVER["DOCUMENT_ROOT"] . "/" . $this->pathResources . $this->componentPath, true, true);
        } else {
            throw new \Bitrix\Main\IO\InvalidPathException(__DIR__ . $this->componentPath);
        }

        if(\Bitrix\Main\IO\Directory::isDirectoryExists(__DIR__ . $this->apiPath)) {
            CopyDirFiles(__DIR__ . $this->apiPath, $_SERVER["DOCUMENT_ROOT"] . $this->apiPath, true, true);
        } else {
            throw new \Bitrix\Main\IO\InvalidPathException(__DIR__ . $this->apiPath);
        }
    }

    public function InstallDB()
    {
        if (CModule::IncludeModule('highloadblock')) {
            $result = Bitrix\Highloadblock\HighloadBlockTable::add(array(
                'NAME' => ucfirst($this->hlTableName),
                'TABLE_NAME' => $this->hlTableName,
            ));
            if ($result->isSuccess()) {
                $hlId = $result->getId();

                $userTypeEntity = new CUserTypeEntity();
                $userTypeDataComment = [
                    'ENTITY_ID' => 'HLBLOCK_' . $hlId,
                    'FIELD_NAME' => 'UF_COMMENT',
                    'USER_TYPE_ID' => 'string',
                    'XML_ID' => 'XML_ID_COMMENT',
                    'SORT' => 500,
                    'MULTIPLE' => 'N',
                    'MANDATORY' => 'Y',
                    'SHOW_FILTER' => 'N',
                    'SHOW_IN_LIST' => '',
                    'EDIT_IN_LIST' => '',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS' => array(
                        'DEFAULT_VALUE' => '',
                        'SIZE' => '20',
                        'ROWS' => '1',
                        'MIN_LENGTH' => '0',
                        'MAX_LENGTH' => '0',
                        'REGEXP' => '',
                    ),
                    'EDIT_FORM_LABEL' => array(
                        'ru' => 'Комментарий',
                        'en' => 'Comment',
                    ),
                    'LIST_COLUMN_LABEL' => array(
                        'ru' => 'Комментарий',
                        'en' => 'Comment',
                    ),
                    'LIST_FILTER_LABEL' => array(
                        'ru' => 'Комментарий',
                        'en' => 'Comment',
                    ),
                    'ERROR_MESSAGE' => array(
                        'ru' => 'Ошибка при заполнении пользовательского свойства ',
                        'en' => 'An error in completing the user field',
                    ),
                    'HELP_MESSAGE' => array(
                        'ru' => '',
                        'en' => '',
                    ),
                ];
                $userTypeDataElement = [
                    'ENTITY_ID' => 'HLBLOCK_' . $hlId,
                    'FIELD_NAME' => 'UF_ID',
                    'USER_TYPE_ID' => 'integer',
                    'XML_ID' => 'XML_ID_ID',
                    'SORT' => 500,
                    'MULTIPLE' => 'N',
                    'MANDATORY' => 'Y',
                    'SHOW_FILTER' => 'N',
                    'SHOW_IN_LIST' => '',
                    'EDIT_IN_LIST' => '',
                    'IS_SEARCHABLE' => 'N',
                    'SETTINGS' => array(
                        'DEFAULT_VALUE' => '',
                        'SIZE' => '20',
                        'ROWS' => '1',
                        'MIN_LENGTH' => '0',
                        'MAX_LENGTH' => '0',
                        'REGEXP' => '',
                    ),
                    'EDIT_FORM_LABEL' => array(
                        'ru' => 'ID Элемента',
                        'en' => 'ID element',
                    ),
                    'LIST_COLUMN_LABEL' => array(
                        'ru' => 'ID Элемента',
                        'en' => 'ID element',
                    ),
                    'LIST_FILTER_LABEL' => array(
                        'ru' => 'ID Элемента',
                        'en' => 'ID element',
                    ),
                    'ERROR_MESSAGE' => array(
                        'ru' => 'Ошибка при заполнении пользовательского свойства ',
                        'en' => 'An error in completing the user field',
                    ),
                    'HELP_MESSAGE' => array(
                        'ru' => '',
                        'en' => '',
                    ),
                ];

                $userTypeEntity->Add($userTypeDataComment);
                $userTypeEntity->Add($userTypeDataElement);
            }
        }

        if (CModule::IncludeModule("iblock")) {
            $ib = new CIBlock;
            $IBLOCK_TYPE = "references";
            $SITE_ID = "s1";
            $arAccess = [
                "2" => "R",
            ];
            $arFields = [
                "ACTIVE" => "Y",
                "NAME" => "Материалы",
                "CODE" => $this->iblockCode,
                "IBLOCK_TYPE_ID" => $IBLOCK_TYPE,
                'IBLOCK_CODE' => $this->iblockCode,
                "SITE_ID" => $SITE_ID,
                "GROUP_ID" => $arAccess,
            ];
            $ID = $ib->Add($arFields);
        }
        return false;
    }

    public function InstallRouter()
    {
        CUrlRewriter::Add($this->urlRules);
    }

    public function UnInstallRouter()
    {
        $sites = CSite::GetList($by="sort", $order="desc", []);
        $site = $sites->Fetch();
        $this->urlRules['SITE_ID'] = $site['LID'];
        unset($this->urlRules['SORT']);
        unset($this->urlRules['RULE']);
        CUrlRewriter::Delete($this->urlRules);
    }
}