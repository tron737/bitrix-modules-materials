<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 13:54
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->IncludeComponent(
    'company:materials',
    '',
    [
        'SEF_FOLDER' => '/materials/',
        'PREVIEW' => [
            'WIDTH' => 128,
            'HEIGHT' => 128,
        ],
        'DETAIL' => [
            'WIDTH' => 512,
            'HEIGHT' => 512,
        ],
    ]
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");