<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 18:46
 */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if(!check_bitrix_sessid()){

    return;
}

echo(CAdminMessage::ShowNote(Loc::getMessage("MATERIALS_UNSTEP_BEFORE")." ".Loc::getMessage("MATERIALS_UNSTEP_AFTER")));
?>

<form action="<? echo($APPLICATION->GetCurPage()); ?>">
    <input type="hidden" name="lang" value="<? echo(LANG); ?>" />
    <input type="submit" value="<? echo(Loc::getMessage("MATERIALS_UNSTEP_SUBMIT_BACK")); ?>">
</form>