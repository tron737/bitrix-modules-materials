<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 18:37
 */

$MESS["MATERIALS_NAME"] = "Модуль вывода материалов и управления комментариями";
$MESS["MATERIALS_DESCRIPTION"] = "Вывод материалов с комментариями";
$MESS["MATERIALS_PARTNER_NAME"] = "Company";
$MESS["MATERIALS_PARTNER_URI"] = "http://site.ru/";
$MESS["MATERIALS_UNINSTALL_TITLE"] = "Деинсталляция модуля";