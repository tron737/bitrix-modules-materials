<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 18:39
 */

$MESS["MATERIALS_UNINSTALL_TITLE"] = "Деинсталляция модуля";
$MESS["MATERIALS_UNSTEP_BEFORE"] = "Модуль";
$MESS["MATERIALS_UNSTEP_AFTER"] = "удален";
$MESS["MATERIALS_UNSTEP_SUBMIT_BACK"] = "Вернуться в список";