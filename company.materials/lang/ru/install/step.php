<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-11
 * Time: 18:39
 */

$MESS["MATERIALS_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["MATERIALS_INSTALL_TITLE"] = "Установка модуля";

$MESS["MATERIALS_STEP_BEFORE"] = "Модуль";
$MESS["MATERIALS_STEP_AFTER"] = "установлен";
$MESS["MATERIALS_STEP_SUBMIT_BACK"] = "Вернуться в список";